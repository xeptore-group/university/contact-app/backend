using System.Text.Json.Serialization;
using Microsoft.AspNetCore.Mvc.ModelBinding;
using System.ComponentModel.DataAnnotations;


namespace ContactApp.Models
{
    public class ContactPhone
    {
        [BindNever]
        public int ID { get; set; }

        [JsonIgnore]
        public int OwnerID { get; set; }

        [JsonIgnore]
        [BindNever]
        public Contact Owner { get; set; }

        [Required]
        [BindRequired]
        public string Number { get; set; }

        [Required]
        [BindRequired]
        public int TypeID { get; set; }

        public ContactPhoneType Type { get; set; }
    }
}