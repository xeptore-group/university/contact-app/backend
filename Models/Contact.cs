using System;
using System.Collections.Generic;
using System.Text.Json.Serialization;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Microsoft.AspNetCore.Mvc.ModelBinding;
using Microsoft.AspNetCore.Http;


namespace ContactApp.Models
{
    public enum ContactType
    {
        Real,
        Legal,
    }


    public class Contact
    {
        [BindNever]
        public int ID { get; set; }

        [Required]
        [BindRequired]
        public string FirstName { get; set; }

        [Required]
        [BindRequired]
        public string LastName { get; set; }

        [Required]
        [BindRequired]
        public ContactType Type { get; set; }

        [BindNever]
        public DateTime DateAdded { get; set; } = DateTime.Now;

        public IEnumerable<ContactPhone> Phones { get; set; }

        [BindNever]
        public IList<ContactImage> Images { get; set; }

        [JsonIgnore]
        [NotMapped]
        public IEnumerable<IFormFile> Pics { get; set; } = new List<IFormFile>();
    }
}
