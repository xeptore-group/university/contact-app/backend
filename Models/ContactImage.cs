using System;
using System.Text.Json.Serialization;
using Microsoft.AspNetCore.Mvc.ModelBinding;


namespace ContactApp.Models
{
    public class ContactImage
    {
        public int ID { get; set; }

        public int OwnerID { get; set; }

        [JsonIgnore]
        public Contact Owner { get; set; }

        [BindNever]
        public DateTime UploadedAt { get; set; } = DateTime.Now;

        public string Path { get; set; }

        public string Name { get; set; }

        public string Ext { get; set; }
    }

}
