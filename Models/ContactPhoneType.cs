using Microsoft.AspNetCore.Mvc.ModelBinding;
using System.ComponentModel.DataAnnotations;


namespace ContactApp.Models
{
    public class ContactPhoneType
    {
        [BindNever]
        public int ID { get; set; }

        [BindRequired]
        [Required]
        public string Name { get; set; }
    }
}