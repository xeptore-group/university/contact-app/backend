using Microsoft.EntityFrameworkCore;
using ContactApp.Models;

namespace ContactApp.Infrastructure
{
    public class ContactAppContext : DbContext
    {
        public ContactAppContext(DbContextOptions<ContactAppContext> options) : base(options) {}

        public DbSet<Contact> Contacts { get; set; }
        public DbSet<ContactPhone> ContactPhones { get; set; }
        public DbSet<ContactPhoneType> ContactPhoneTypes { get; set; }
        public DbSet<ContactImage> ContactImages { get; set; }
    }
}
