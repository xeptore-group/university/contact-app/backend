﻿using System.Linq;
using ContactApp.Models;
using ContactApp.Infrastructure;

namespace ContactApp.Data
{
    public class DbInitializer
    {
        public static void Initialize(ContactAppContext context)
        {
            context.Database.EnsureCreated();

            if (context.Contacts.Any())
            {
                return;
            }

            var phoneTypes = new ContactPhoneType[]
            {
                new ContactPhoneType{Name="Home"},
                new ContactPhoneType{Name="Work"},
                new ContactPhoneType{Name="IR-MCI"},
                new ContactPhoneType{Name="Irancell"},
            };
            context.ContactPhoneTypes.AddRange(phoneTypes);


            var contacts = new Contact[]
            {
                new Contact{
                    FirstName="Carson",
                    LastName="Alexander",
                    Type=ContactType.Real,
                    Phones=new ContactPhone[]
                    {
                        new ContactPhone
                        {
                            Number="9371296909",
                            Type=phoneTypes[3]
                        },
                        new ContactPhone
                        {
                            Number="9123456789",
                            Type=phoneTypes[2]
                        }
                    }
                },
                new Contact{
                    FirstName="Meredith",
                    LastName="Alonso",
                    Type=ContactType.Legal,
                    Phones=new ContactPhone[]
                    {
                        new ContactPhone
                        {
                            Number="5632422565",
                            Type=phoneTypes[0]
                        },
                        new ContactPhone
                        {
                            Number="5632451295",
                            Type=phoneTypes[1]
                        },
                        new ContactPhone
                        {
                            Number="9134567892",
                            Type=phoneTypes[2]
                        }
                    }
                },
                new Contact{
                    FirstName="Meredith",
                    LastName="Alonso",
                    Type=ContactType.Real,
                    Phones=new ContactPhone[]
                    {
                        new ContactPhone
                        {
                            Number="5632451295",
                            Type=phoneTypes[1]
                        },
                        new ContactPhone
                        {
                            Number="9134567892",
                            Type=phoneTypes[2]
                        }
                    }
                },
            };
            context.Contacts.AddRange(contacts);

            context.SaveChanges();
        }
    }
}
