﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Microsoft.EntityFrameworkCore;
using System.IO;
using Microsoft.AspNetCore.Hosting;
using ContactApp.Infrastructure;
using ContactApp.Models;


namespace ContactApp.Controllers
{
    [ApiController]
    [Route("contacts")]
    public class ContactsController : ControllerBase
    {
        private readonly ILogger<ContactsController> _logger;
        private readonly ContactAppContext _context;
        private readonly string _wwwroot;


        public ContactsController(
            IWebHostEnvironment hostingEnvironment,
            ILogger<ContactsController> logger,
            ContactAppContext context
            )
        {
            _logger = logger;
            _context = context;
            _wwwroot = Path.Combine(hostingEnvironment.WebRootPath);
        }

        [HttpPost]
        public async Task<IActionResult> AddContact([FromForm]Contact form)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(new { message = "invalid form provided" });
            }

            var images = new List<ContactImage>();
            foreach (var file in form.Pics)
            {
                if (file.Length <= 0) continue;
                if (!new string[] { "image/png", "image/jpg", "image/jpeg" }.Contains(file.ContentType))
                {
                    return BadRequest(new { message = "image type not supported" });
                }

                var ext = file.ContentType switch
                {
                    "image/png" => ".png",
                    "image/jpeg" => ".jpg",
                    "image/jpg" => ".jpg",
                    _ => throw new ArgumentException(message: "invalid image extension value", paramName: nameof(file.ContentType)),
                };


                var uploadPath = Path.Combine(_wwwroot, "uploads", $"{Guid.NewGuid().ToString()}${ext}");

                Directory.CreateDirectory(Path.GetDirectoryName(Path.Combine(uploadPath)));

                Path.GetRelativePath(_wwwroot, uploadPath);

                using (var fileStream = new FileStream(uploadPath, FileMode.Create))
                {
                    await file.CopyToAsync(fileStream);
                }

                images.Add(new ContactImage
                {
                    Ext = ext,
                    Name = file.FileName,
                    Path = Path.GetRelativePath(_wwwroot, uploadPath)
                });
            }

            form.Images = new List<ContactImage>().Union(images).ToList();

            _context.Contacts.Add(form);
            await _context.SaveChangesAsync();

            return CreatedAtAction(nameof(GetContact), new { ID = form.ID }, form);
        }

        [HttpPut("{id}")]
        public async Task<IActionResult> EditContact([FromRoute]int ID, [FromForm]Contact contact)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(new { message = "invalid form provided" });
            }

            if (!await _context.Contacts.AnyAsync(c => c.ID == ID))
            {
                return NotFound(new { message = "contact is not registered" });
            }

            var images = new List<ContactImage>();
            foreach (var file in contact.Pics)
            {
                if (file.Length <= 0) continue;
                if (!new string[] { "image/png", "image/jpg", "image/jpeg" }.Contains(file.ContentType))
                {
                    return BadRequest(new { message = "image type not supported" });
                }

                var ext = file.ContentType switch
                {
                    "image/png" => ".png",
                    "image/jpeg" => ".jpg",
                    "image/jpg" => ".jpg",
                    _ => throw new ArgumentException(message: "invalid image extension value", paramName: nameof(file.ContentType)),
                };

                var uploadPath = Path.Combine(_wwwroot, "uploads", $"{Guid.NewGuid().ToString()}${ext}");

                Directory.CreateDirectory(Path.GetDirectoryName(Path.Combine(uploadPath)));

                Path.GetRelativePath(_wwwroot, uploadPath);

                using (var fileStream = new FileStream(uploadPath, FileMode.Create))
                {
                    await file.CopyToAsync(fileStream);
                }

                images.Add(new ContactImage
                {
                    Ext = ext,
                    Name = file.FileName,
                    Path = Path.GetRelativePath(_wwwroot, uploadPath),
                    UploadedAt = DateTime.Now,
                });
            }

            if (images.Count > 0)
            {
                contact.Images = images;
            }

            var contactPhoneIDs = await _context.ContactPhones.Where(ph => ph.OwnerID == ID).ToListAsync();
            _context.ContactPhones.RemoveRange(contactPhoneIDs);

            contact.ID = ID;

            _context.Contacts.Update(contact);
            await _context.SaveChangesAsync();

            return NoContent();
        }

        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteContact([FromRoute]int ID)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(new { message = "invalid form provided" });
            }

            if (!await _context.Contacts.AnyAsync(c => c.ID == ID))
            {
                return NotFound(new { message = "contact is not registered" });
            }

            var contactPhoneIDs = await _context.ContactPhones.Where(ph => ph.OwnerID == ID).ToListAsync();
            _context.ContactPhones.RemoveRange(contactPhoneIDs);

            _context.Contacts.Remove(new Contact { ID = ID });
            await _context.SaveChangesAsync();

            return NoContent();
        }

        [HttpGet]
        public async Task<IActionResult> GetContacts()
        {
            var contacts = await _context.Contacts.Include(c => c.Images).ToListAsync();
            return Ok(contacts);
        }

        [HttpGet("{id}")]
        public async Task<IActionResult> GetContact([FromRoute]int ID)
        {
            if (!ModelState.IsValid)
            {
                return NotFound(new { message = "contact is not registered" });
            }

            var contact = await _context.Contacts
                .Include(c => c.Phones)
                .ThenInclude(cp => cp.Type)
                .Include(c => c.Images)
                .FirstOrDefaultAsync(c => c.ID == ID);
            if (contact == null)
            {
                return NotFound(new { message = "contact is not registered" });
            }

            return Ok(contact);
        }
    }
}
