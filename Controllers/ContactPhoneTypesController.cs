using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Microsoft.EntityFrameworkCore;
using ContactApp.Infrastructure;
using ContactApp.Models;


namespace ContactApp.Controllers
{
    [ApiController]
    [Route("contact-phone-types")]
    public class ContactPhoneTypesController : ControllerBase
    {
        private readonly ILogger<ContactPhoneTypesController> _logger;
        private readonly ContactAppContext _context;

        public ContactPhoneTypesController(
            ILogger<ContactPhoneTypesController> logger,
            ContactAppContext context
            )
        {
            _logger = logger;
            _context = context;
        }

        [HttpPost]
        public async Task<IActionResult> AddPhoneType([FromBody]ContactPhoneType phoneType)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(new { message = "invalid form provided" });
            }

            if (await _context.ContactPhoneTypes.AnyAsync(pt => pt.Name == phoneType.Name)) {
                return BadRequest(new { message = "phone type already registered" });
            }

            _context.ContactPhoneTypes.Add(phoneType);
            await _context.SaveChangesAsync();

            return CreatedAtAction(nameof(GetPhoneType), new { ID = phoneType.ID }, phoneType);
        }

        [HttpPut("{id}")]
        public async Task<IActionResult> EditPhoneType([FromRoute]int ID, [FromBody]ContactPhoneType phoneType)
        {
            if (!ModelState.IsValid || string.IsNullOrEmpty(phoneType.Name))
            {
                return BadRequest(new { message = "invalid form provided" });
            }

            if (!await _context.ContactPhoneTypes.AnyAsync(c => c.ID == ID))
            {
                return NotFound(new { message = "phone type is not registered" });
            }

            phoneType.ID = ID;
            _context.ContactPhoneTypes.Update(phoneType);
            await _context.SaveChangesAsync();

            return NoContent();
        }

        [HttpGet]
        public async Task<IActionResult> GetPhoneTypes()
        {
            var phoneTypes = await _context.ContactPhoneTypes.ToListAsync();
            return Ok(phoneTypes);
        }

        [HttpGet("{id}")]
        public async Task<IActionResult> GetPhoneType([FromRoute]int ID)
        {
            if (!ModelState.IsValid)
            {
                return NotFound(new { message = "phone type is not registered" });
            }

            var phoneType = await _context.ContactPhoneTypes.FirstOrDefaultAsync(c => c.ID == ID);
            if (phoneType == null)
            {
                return NotFound(new { message = "phone type is not registered" });
            }

            return Ok(phoneType);
        }
    }
}
